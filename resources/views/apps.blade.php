<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from demo.epic-webdesign.com/tf-howdy/v5/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Oct 2018 05:44:18 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">

     <title>@yield('title')</title>
    
    <!-- Loading Bootstrap -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Loading Template CSS -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/footer.css') }}" rel="stylesheet">
    <link href="{{asset('css/style-magnific-popup.css')}}" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cabin:500,600,700" rel="stylesheet">

    <!-- Awsome Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/pe-icon-7-stroke.css') }}">

	<!-- Optional - Adds useful class to manipulate icon font display -->
	<!-- <link rel="stylesheet" href="{{ asset('css/helper.css') }}"> -->

	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
	
    <!-- Font Favicon -->
    <link rel="shortcut icon" href="{{asset('img/v5/favicon.ico')}}">
        
    
</head>

<body>

    @section('navbar')
    @show

  <section class="home-section" id="home_wrapper">
    @yield('home')
  </section>
  <section class="section-grey" id="about">
    @yield('about')
  </section>
  <div class="footer">
    @yield('footer')
  </div>
</body>
  <script src="{{asset('js/jquery.js')}}"></script>
  <script src="{{asset('js/bootstrap.js')}}"></script>
  <script src="{{asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('js/jquery.scrollTo-min.js')}}"></script>
  <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('js/jquery.nav.js')}}"></script>
  <script src="{{asset('js/plugins.js')}}"></script>
  <script src="{{asset('js/custom.js')}}"></script>

</html>