<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from mintone.xyz/index-projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Nov 2018 03:13:52 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Admin Mintone">
<meta name="author" content="Admin Mintone">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="assets/dash/favicon.png">
<title>@yield('title')</title>
<!-- Bootstrap Core CSS -->
<link href="{{asset('css/bootstrap4.css')}}" rel="stylesheet">
<link href="{{asset('css/dashboard/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet">
<!-- This page CSS -->
<!-- chartist CSS -->
<link href="{{asset('css/dashboard/morrisjs/morris.css')}}" rel="stylesheet">
<!--c3 CSS -->
<link href="{{asset('css/dashboard/c3-master/c3.min.css')}}" rel="stylesheet">
<!--Toaster Popup message CSS -->
<link href="{{asset('css/dashboard/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
<!-- page css -->
<link href="{{asset('css/dashboard/pages/tab-page.css')}}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{asset('css/dashboard/style.css')}}" rel="stylesheet">
<link href="{{asset('css/dashboard/responsive.css')}}" rel="stylesheet">
<link href="{{asset('css/dashboard/bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet" type="text/css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="fix-header fix-sidebar card-no-border">
    @yield('preload')
    @yield('content')
</body>
<script src="{{asset('js/jquery.331.js')}}"></script>
<!-- Bootstrap popper Core JavaScript -->
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script>
function toggleIcon(e) {
    $(e.target)
        .prev('.card-link')
        .find(".more-less")
        .toggleClass('fa-plus fa-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('js/dashboard/perfect-scrollbar.jquery.min.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('js/dashboard/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('js/dashboard/sidebarmenu.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('js/dashboard/custom.min.js')}}"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!--c3 JavaScript -->
<script src="{{asset('js/dashboard/d3.min.js')}}"></script>
<script src="{{asset('js/dashboard/c3.min.js')}}"></script>
<!--jquery knob -->
<script src="{{asset('js/dashboard/jquery.knob.js')}}"></script>
<!--Morris JavaScript -->
<script src="{{asset('js/dashboard/raphael-min.js')}}"></script>
<script src="{{asset('js/dashboard/morris.js')}}"></script>
<!-- Popup message jquery -->
<script src="{{asset('js/dashboard/jquery.toast.js')}}"></script>
<!-- Dashboard JS -->
<script src="{{asset('js/dashboard/dashboard-projects.js')}}"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bdeb7964cfbc9247c1ea1f3/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<!-- <script src="plugins/vendors/styleswitcher/jQuery.style.switcher.js"></script>
<script src="plugins/vendors/datatables/jquery.dataTables.min.js"></script>
<script>
   $(function() {
       $('#myTable').DataTable();
           var table = $('#example').DataTable({
              "columnDefs": [{
                  "visible": false,
                  "targets": 2
            }],
            "order": [
                  [2, 'asc']
            ],
            "displayLength": 8,
             "drawCallback": function(settings) {
                var api = this.api();
                 var rows = api.rows({
                     page: 'current'
                 }).nodes();
                var last = null;
             api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script> -->
<script>
$('#slimtest1, #slimtest2, #slimtest3, #slimtest4').perfectScrollbar();
</script>
<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.9"></script>
<script>
    var options = {
        strings: ["{{ Auth::user()->name }},^500 Selamat datang di Printy!^800","^800Selamat Berbelanja."],
        typeSpeed: 50,
        backSpeed: 50,
        startDelay: 800,
        loop: true
    }
    var typed = new Typed(".welcome", options);
</script>
</body>
<script src="{{asset('js/dashboardapp.js')}}"></script>
<!-- Mirrored from mintone.xyz/index-projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Nov 2018 03:14:08 GMT -->
</html>

