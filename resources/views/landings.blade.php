@extends('apps')
@section('title', 'Printy Indonesia')
@section('navbar')
<!--begin header -->
<header class="header">
<!--begin nav -->
<nav class="navbar navbar-default navbar-fixed-top">
    
    <!--begin container -->
    <div class="container">

        <!--begin navbar -->
        <div class="navbar-header">

            <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                
            <!--logo -->
            <a href="http://printy.id/" class="navbar-brand" id="logo"><img src="{{asset('img/logo.png')}}"></a>

        </div>
                
        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">

                <li><a href="#home_wrapper"><i class="fas fa-home"></i>Beranda</a></li>

                <!-- <li><a href="#about">Cara Kerja</a></li> -->

                <li><a href="/login" class="discover-btn">Daftar</a></li>
               
            </ul>
        </div>
        <!--end navbar -->
                            
    </div>
    <!--end container -->
    
</nav>
<!--end nav -->

</header>
<!--end header -->
@endsection
@section('home')
		<!--begin container -->
		<div class="container">

	        <!--begin row -->
	        <div class="row">
	          
	            <!--begin col-md-6-->
	            <div class="col-md-12 padding-top-50">

	          		<h1>Selamat Datang di Printy!</h1>

	          		<p>Layanan cetak online pertama di Indonesia yang mengusung konsep unggah - cetak - kirim</p>

					<a href="#about" class="btn-white-border scrool">Bagaimana Kerja nya?</a>

	          		<img src="{{asset('img/v5/hero01.png')}}" class="width-100" alt="image">

	            </div>
	            <!--end col-md-6-->
	       
	        </div>
	        <!--end row -->

		</div>
		<!--end container -->
@endsection
@section('about')
		<!--begin container -->
		<div class="container">

	        <!--begin row -->
	        <div class="row">

				<!--begin col-md-12 -->
				<div class="col-md-12 text-center">

					<h2 class="section-title">Cara Kerja Printy</h2>

					<p class="section-subtitle">Unggah - Cetak - Kirim </p>
					
				</div>
				<!--end col-md-12 -->

				<!--begin col-md-4 -->
				<div class="col-md-4">

					<div class="main-services">

						<img src="{{asset('img/v5/main-service1.png')}}" class="width-100" alt="pic">

						<h3>Unggah</h3>

						<p>Curabitur quam etsum lacus etsumis nulat iaculis ets vitae etsum nisle varius sed aliquam ets vitae dictis netsum.</p>
						
					</div>

				</div>
				<!--end col-md-4 -->

				<!--begin col-md-4 -->
				<div class="col-md-4">

					<div class="main-services">

						<img src="{{asset('img/v5/main-service2.png')}}" class="width-100" alt="pic">

						<h3>Cetak</h3>

						<p>Curabitur quam etsum lacus etsumis nulat iaculis ets vitae etsum nisle varius sed aliquam ets vitae dictis netsum.</p>
						
					</div>

				</div>
				<!--end col-md-4 -->

				<!--begin col-md-4 -->
				<div class="col-md-4">

					<div class="main-services">

						<img src="{{asset('img/v5/main-service3.png')}}" class="width-100" alt="pic">

						<h3>Kirim</h3>

						<p>Curabitur quam etsum lacus etsumis nulat iaculis ets vitae etsum nisle varius sed aliquam ets vitae dictis netsum.</p>
						
					</div>

				</div>
				<!--end col-md-4 -->
	       
			</div>
			<div class="row">
				<div class="col-md-12 mt-5 text-center">
					<a href="#" class="btn btn-lg btn-outline-primary">Order Sekarang <i class="fas fa-arrow-circle-right"></i></a>
				</div>
			</div>
	        <!--end row -->
			
		</div>
		<!--end container -->
@endsection
@section('footer')           
            <!--begin container -->
            <div class="container">
                <!--begin row -->
                <div class="row">
                    <div class="col-md-4">
                        <h5 class="addr">Printy Indonesia</h5>
                        <p><i class="fas fa-building"></i> Jl.Telekomunikasi No. 1, Ters. Buah Batu, Bandung, 40257</p>
                        <p><i class="fab fa-whatsapp"></i> +62878 8494 1839 (WhatsApp Only)</p>
                        <p><i class="fas fa-envelope"></i> printy.id@gmail.com</p>
                    </div>
                    <!--begin col-md-12 -->
					<div class="col-md-4">
						<h5 class="addr">Links</h5>
						<ul class="nav foot-nav">
							<li><a href="#">Tentang Printy</a></li>
							<li><a href="#">F.A.Q</a></li>
							<li><a href="#">Karir</a></li>
						</ul>
					</div>
                    <div class="col-md-4 text-center">
                       
                                             
                        <!--begin footer_social -->
                        <ul class="footer_social mt-5">
    
                            <li>
                                <a href="http://instagram.com/printyid" target="_blank">
                                    <i class="fab fa-instagram" style="font-size:72px !important;"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://wa.me/6287884941839?text=Halo%20Printy!">
                                    <i class="fab fa-whatsapp" style="font-size:72px !important;"></i>
                                </a>
                            </li>
    
                        </ul>
                        <p class="mt-5">Copyright © <?php echo date('Y');?> Printy Indonesia. All Right Reserved</p>
                        <!--end footer_social -->
                        
                    </div>
					
                    <!--end col-md-6 -->
                    
                </div>
                <!--end row -->
                
            </div>
            <!--end container -->
@endsection