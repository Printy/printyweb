@extends('login.layout.logapp')
@section('logform')
<div class="of-elegant-modal">

<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-6 p-4 of-intro-container">
							<a href='/'><img src="{{asset('img/logo.png')}}" class="img-fluid"></a>
						</div> <!--End .col-lg-6 -->
						<div class="col-lg-6 px-0">
							<div class="py-4 of-login-container of-show">
								<h3 class="pt-2">Halo.</h3>
								<span class="of-form-description typed"></span>
								<form class="mt-3" action="{{route('login')}}" method="post">
									@csrf
									<span class="eror ">{{$errors->first('email')}}</span>
									<div class="form-group">
										<label for="login-email">Email</label>
										<div class="of-input-container">
											<div class="of-input-icon"><img src="{{asset('img/login/mail.svg')}}"></div>
											<input type="text" class="form-control of-email-validate of-login-email" id="login-email" name="email" placeholder="Email" spellcheck="false">
											<div class="of-input-validation"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="login-password">Password</label>
										<div class="of-input-container">
											<div class="of-input-icon"><img src="{{asset('img/login/lock.svg')}}"></div>
											<input type="password" name="password" class="form-control of-login-password " id="login-password" placeholder="Password" spellcheck="false">
											<div class="of-input-validation lpassword-toggle-btn show-password"></div>
											
										</div>
									</div>
									<div class="container mb-4 pt-3">
										<div class="row">
											<div class="col-6">
												<!-- <label class="control control--checkbox">First checkbox
													<input type="checkbox" checked="checked"/>
													<div class="control__indicator"></div>
												</label> -->
											</div> <!-- End .col-6 -->
											<div class="col-6 text-right">
												<a class="of-forgot-password of-forgot-link" href="#">Forgot password?</a>
											</div> <!-- End .col-6 -->
										</div> <!-- End .row -->
									</div> <!-- End .container -->
									<div class="container">
										<div class="row">
											<div class="col-lg-6 mb-4 of-login-btn-container">
												<button type="submit" class="btn of-login-btn btn-primary" disabled>Sign In</button>
											</div> <!-- End .col-lg-6 -->
										</div> <!-- End .row -->
									</div> <!-- End .container -->
								</form>
								<a class="of-signup-link of-toggle-link pb-2 cursor">Belum punya akun? Daftar disini!</a>
							</div> <!-- End .of-login-container -->
							<div class="py-4 of-forgot-container">
								<h3 class="pt-2 pb-3">Forgot your password?</h3>
								<p class="of-form-description">Enter your email below to receive your password reset instructions.</p>
								<form class="pt-3">
									<div class="form-group">
										<label for="login-email">Email address</label>
										<div class="of-input-container">
											<div class="of-input-icon"><img src="{{asset('img/login/mail.svg')}}"></div>
											<input type="text" class="form-control of-email-validate of-forgot-email" id="forgot-email" name="of-forgot-email" placeholder="Email" spellcheck="false">
											<div class="of-input-validation"></div>
										</div>
									</div>
									<div class="form-group">
										<button type="submit" class="btn of-block-btn of-forgot-btn btn-primary btn-block" disabled>Send Password</button>
									</div> <!-- End .container -->
								</form>
								<a class="of-toggle-link of-forgot-link pb-2 cursor">Back</a>
							</div> <!--End .of-forgot-container -->
							<div class="py-4 of-register-container">
								<p class="of-form-description"><b>Printy Account</b></p>
								<form action="{{ route('register') }}" method="POST">
								@csrf
								
									<div class="container-fluid">
										<div class="row">
											<div class="form-group col-6 pl-0">
												<label for="first-name">Nama Lengkap</label>
												<div class="of-input-container">
													<input type="text" name="name" class="form-control of-register-name no-icon" id="first-name" placeholder="Nama Lengkap" spellcheck="false">
													<div class="of-input-validation"></div>
												</div>
											</div> <!--End .col-6 -->
											<div class="form-group col-6 pl-0">
                                                <label for="register-email">Email</label>
                                                <div class="of-input-container">
                                                    <div class="of-input-icon"><img src="{{asset('img/login/mail.svg')}}"></div>
                                                    <input type="text" name="email" class="form-control of-email-validate of-register-email" id="register-email" placeholder="Email" spellcheck="false">
                                                    <div class="of-input-validation"></div>
                                                </div>
											</div> <!--End .col-6 -->
										</div> <!--End .row -->
									</div> <!--End .container -->
									<!-- <div class="form-group">
										<label for="register-email">Email</label>
										<div class="of-input-container">
											<div class="of-input-icon"><img src="{{asset('img/login/mail.svg')}}"></div>
											<input type="text" class="form-control of-email-validate of-register-email" id="register-email" placeholder="Enter Email..." spellcheck="false">
											<div class="of-input-validation"></div>
										</div>
									</div> End .form-group -->
									<div class="form-group pr-0">
										<label for="register-password">Set Password</label>
										<div class="of-input-container">
											<div class="of-input-icon"><img src="{{asset('img/login/lock.svg')}}"></div>
											<input type="password" name="password" class="form-control password" id="register-password" placeholder="Password" spellcheck="false">
											
										</div>
									</div><!--End .form-group -->
									<div class="form-group pr-0">
										<label for="register-password">Set Password</label>
										<div class="of-input-container">
											<div class="of-input-icon"><img src="{{asset('img/login/lock.svg')}}"></div>
											<input id="password-confirm" type="password" class="form-control password" name="password_confirmation" required>
											<div class="of-input-validation rpassword-toggle-btn show-password"></div>
										</div>
									</div><!--End .form-group -->
									<div class="form-group">
										<button type="submit" class="btn of-block-btn of-register-btn btn-primary btn-block">Daftar</button>
                                    </div> <!-- End .container -->
                                    <a class="of-signin-link of-toggle-link cursor">Sudah punya akun? Login!</a>
								</form>
								
							</div> <!--End .of-register-container -->
						</div> <!--End .col-lg-6 -->
					</div> <!--End .row -->
                </div> <!--End .container-fluid -->
</div>
@endsection