<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from teamof.net/elegant-modal-form/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 28 Oct 2018 02:45:32 GMT -->
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!-- <link rel="stylesheet" href="{{asset('css/demo.css')}}"> -->
  <link rel="stylesheet" href="{{asset('css/login.css')}}">
  <title>Printy Indonesia - Login</title>
</head>

<body>
    @yield('logform')
</body>
<script src="{{asset('js/bundle.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.9"></script>
<script>
    var options = {
        strings: ["Selamat datang di Printy!","Selamat datang di Printy! Selamat Berbelanja."],
        typeSpeed: 40,
        backSpeed: 40,
        startDelay: 800,
        loop: true
    }
    var typed = new Typed(".typed", options);
</script>
</html>